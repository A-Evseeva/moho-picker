-- **************************************************
-- Provide Moho with the name of this script object
-- **************************************************

ScriptName = "RS_Picker"

-- **************************************************
-- General information about this script
-- **************************************************

RS_Picker = {}

function RS_Picker:Name()
	return 'Picker'
end

function RS_Picker:Version()
	return '1.0'
end

function RS_Picker:UILabel()
	return 'Picker'
end

function RS_Picker:Creator()
	return 'Revival Studio'
end

function RS_Picker:Description()
	return ''
end

function RS_Picker:ColorizeIcon()
	return true
end

function RS_Picker:LoadPrefs(prefs)
	local zoom = prefs:GetFloat("RS_Picker.zoom", 1)
	local x = prefs:GetFloat("RS_Picker.panX", 0)
	local y = prefs:GetFloat("RS_Picker.panY", 0)
	self.screenMode = prefs:GetBool("RS_Picker.screenMode")
	self.matrix = LM.Matrix:new_local()
	self.matrix:Translate(x,y,0)
	self.matrix:Scale(zoom, zoom, 1)
	--TODO: also load default UI scheme filename
end

function RS_Picker:SavePrefs(prefs)
	prefs:SetBool("RS_Picker.screenMode", self.screenMode)
	local zeroVec = LM.Vector2:new_local()
	local normVec = LM.Vector2:new_local()
	normVec:Set(1,0)
	self.matrix:Transform(zeroVec)
	self.matrix:Transform(normVec)
	prefs:SetFloat("RS_Picker.panX",zeroVec.x)
	prefs:SetFloat("RS_Picker.panY",zeroVec.y)
	prefs:SetFloat("RS_Picker.zoom",(normVec - zeroVec):Mag())
end

function RS_Picker:ResetPrefs(prefs)
	self.zoom = 1.0
	self.pan = LM.Vector2:new_local()
	self.screenMode = false
	self.matrix = LM.Matrix:new_local()
end

-- **************************************************
-- Is Relevant / Is Enabled
-- **************************************************

function RS_Picker:IsRelevant(moho)
	if not self.resourcePath then
		local slash = string.sub(moho:UserAppDir(), -9, -9)
		self.resourcePath = moho:UserAppDir() .. slash .. "Scripts" .. slash .. "ScriptResources" .. slash 
	end
	return true
end

function RS_Picker:IsEnabled(moho)
	return true
end

-- **************************************************
-- Keyboard/Mouse Control
-- **************************************************

function RS_Picker:NonDragMouseMove()
	return true
end

function RS_Picker:OnMouseMoved(moho, mouseEvent)
	--iterate each of self.controls checking each for is mouse over it and changing it's over state correspondingly
	
	local g = mouseEvent.view:Graphics()
	
	g:Clear(0, 0, 0, 0)
	g:BeginPicking(mouseEvent.pt, 4)
	
	g:Push()
	self:PrepareScreen(g)

	for i, val in pairs(self.controls or {}) do 
		val:Draw(g)
		local previousState = val.over
		if g:Pick() then 
			val.over = true
		else 
			val.over = false
		end
		if previousState ~= val.over then self.need_redraw = true end
	end
	
	g:Pop()
	if self.need_redraw then MOHO.Redraw() end

	
end

function RS_Picker:OnMouseDown(moho, mouseEvent)
	--for controls with over = true set pressed to true
	for i, val in pairs(self.controls or {}) do
		if val.over then val.pressed = true end
	end	
end

function RS_Picker:OnMouseUp(moho, mouseEvent)
	--for controls with pressed = true set pressed to false and call Eval
	local somethingChanged = false
	for i, val in pairs(self.controls or {}) do
		if val.over then
			val:select()
			val.pressed = false
			val:Eval(moho)
			somethingChanged = true
		end
	end
	if somethingChanged then 
		MOHO.Redraw()
	end
end

function RS_Picker:OnKeyDown(moho, keyEvent)
	
end

function RS_Picker:OnKeyUp(moho, keyEvent)
	
end

function RS_Picker:OnInputDeviceEvent(moho, deviceEvent)
	
end

function RS_Picker:DrawMe(moho, view)

	local g = view:Graphics()
	g:Push()

	self:PrepareScreen(g)

	--iterate each of self.controls calling Draw method for each of them
	for i, val in pairs(self.controls or {}) do val:Draw(g) end
	
	g:Pop()

end

-- **************************************************
-- Tool Panel Layout
-- **************************************************

RS_Picker.RESET_ZOOM = MOHO.MSG_BASE

function RS_Picker:DoLayout(moho, layout)
	self.reset_zoomButton = LM.GUI.ImageButton("TimelineExpanderUp", "Fix to screen", true, self.RESET_ZOOM, true)
    --self.reset_zoomButton = LM.GUI.Button('Reset zoom', self.RESET_ZOOM)
    layout:AddChild(self.reset_zoomButton, LM.GUI.ALIGN_LEFT, 0)
	--TODO: change the way zoom and pan tuned: make a check-button whish swithces absolute/relative transform of view-GUI and fixes zoom and pan at the time of switc

	if not RS_Picker.controls then
		--TODO: get one of multiple saved schemes selected with tool UI
		--for now the base scheme name is hardcoded here:	
		
		local schemePath = self.resourcePath .. "default.scheme"
		local read_lines = ""
		for line in io.lines(schemePath) do read_lines = read_lines .. line end
		local runner, err = load("return " .. read_lines)
		if err then return (print(err)) end
		local list = runner()
		RS_Picker.controls = {}
		for i, val in pairs(list) do
			if getmetatable(val) then 
				table.insert(RS_Picker.controls, val)
			else
				for j, radio in pairs(val) do
					radio.radios = val
					table.insert(RS_Picker.controls, radio)
				end
			end
		end
		
		RS_Picker.need_redraw = true
	end
	
end

function RS_Picker:HandleMessage(moho, view, msg)
    if msg == self.RESET_ZOOM then

		local g = view:Graphics()
		local m = g:CurrentTransform()
		
		if self.screenMode then m:Invert() end
		m:Multiply(self.matrix)
		self.matrix:Set(m)
		
		--self.screenMode = not self.screenMode
		self.screenMode = self.reset_zoomButton:Value()
    else
        
    end
end

function RS_Picker:UpdateWidgets(moho)
	RS_Picker.reset_zoomButton:SetValue(self.screenMode)
end

-- **************************************************
-- Object-oriented control classes, base and derived
-- **************************************************

PickerControl = {}

PickerControl.SHAPE_CIRCLE = 1
PickerControl.SHAPE_DIAMOND = 2
PickerControl.SHAPE_UP = 3

function PickerControl:new(position, shape, color, method, args)

    local obj= {}
	
    -- метод
    function PickerControl:Draw(g, filled) --TODO: provide it with args needed to use Graphics
		if not self.enabled then return end
		
		local R, G, B = table.unpack(self.color)
		local alpha = filled or 255
		g:SetColor(R, G, B, alpha)
		if self.points then 
			self:DrawPoints(g, self.points, filled)
		else
			--draw shape set as shape constant (circle, diamond and so on)
			if self.shape == PickerControl.SHAPE_CIRCLE then
				if filled then 
					g:FillCircle(self.position, 0.05)
				else
					g:FrameCircle(self.position, 0.05)
				end
				
			elseif self.shape == PickerControl.SHAPE_DIAMOND then
				g:DrawDiamondMarker(self.position.x, self.position.y, 0.05)
			elseif self.shape == PickerControl.SHAPE_UP then
				local v = LM.Vector2:new_local()
				v:Set(0,0.05)
				local tempPoints = {}
				for i=0, 3 do
					v:Rotate(math.pi*2/3)
					table.insert(tempPoints, {x=(self.position.x + v.x), y=(self.position.y + v.y)})
				end
				self:DrawPoints(g, tempPoints, filled)
			end
		end
    end	
	
	function obj:LoadPoints(shape_name)
		--TODO: update this and RS_Picker:ExportShape method (and also create RS_Picker:ImportShape method) to have ability to save mutiple curves in one text file and load them by their names saved there too
		
		local picPath = RS_Picker.resourcePath .. shape_name
		local read_lines = ""
		for line in io.lines(picPath) do read_lines = read_lines .. line end
		local runner, err = load("return " .. read_lines)
		if err then return (print(err)) end
		local temporal = runner() 
		
		obj.points = {}
		for i,val in pairs(temporal) do
			table.insert(obj.points, {x=val[1], y=val[2]})
		end
	end	
	
	function PickerControl:DrawPoints(g, points, filled)
		-- use loaded points array to draw lines
		if filled then
			local v1 = LM.Vector2:new_local()
			local v2 = LM.Vector2:new_local()
			g:BeginShape()
			for i,val in pairs(points) do
				local j = i + 1
				if j > #points then j = 1 end
				v1:Set(points[i].x, points[i].y)
				v2:Set(points[j].x, points[j].y)
				g:AddLine(v1, v2)
			end
			g:EndShape()
		else 
			for i,val in pairs(points) do
				if i > 1 then 
					g:DrawLine(
						points[i-1].x,
						points[i-1].y,
						val.x,
						val.y
						)
				end		
			end
		end
	end	
	
	function PickerControl:select()
		self.selected = not self.selected
	end
	
	--абстрактный метод
	function PickerControl:Eval(moho)

	end	
		
	--свойства (типа, конструктор)
        obj.position =  LM.Vector2:new_local() -- default is {0,0}
		if position then obj.position:Set(table.unpack(position)) end
		obj.shape = shape or self.SHAPE_CIRCLE
		if type(obj.shape) == "string" then obj:LoadPoints(obj.shape) end
		obj.color = {255, 0, 0} or color -- default is red
		obj.method = method
		obj.args = args
		
		obj.over = false
		obj.selected = false
		obj.pressed = false
		obj.enabled = true
		
    --чистая магия!
    setmetatable(obj, self)
    self.__index = self; return obj
end

PickerButton = {}
--наследуемся
setmetatable(PickerButton,{__index = PickerControl}) 

--переопределяем методы
function PickerButton:Draw(g, filled)
	-- set line width depending on self.over 
	if self.over then g:SetPenWidth(4) else g:SetPenWidth(1) end
	--TODO: and do something (offset?..) for during mouse pressing for self.pressed state
	-- and then call parent method
	PickerControl.Draw(self, g)
	PickerControl.Draw(self, g, filled or 1)

end

function PickerButton:Eval(moho)
	moho.document:PrepUndo(moho.layer)
	moho.document:SetDirty()
	self.method(moho, table.unpack(self.args))
end

PickerStateButton = {}
--наследуемся
setmetatable(PickerStateButton,{__index = PickerButton})
--переопределяем методы
function PickerStateButton:Draw(g, filled)
	filled = self.selected and 255 or 1	
	PickerButton.Draw(self, g, filled)
end

--TODO: implement radio buttons some way
PickerRadioButton = {}
setmetatable(PickerRadioButton,{__index = PickerStateButton})
function PickerRadioButton:select()
	if self.selected then return end	
	for i, val in pairs(self.radios) do
		val.selected = false
	end
	self.selected = true
end

-- **************************************************
-- Additional functions
-- **************************************************

function RS_Picker:PrepareScreen(g)
	if self.screenMode then
		local matrix = g:CurrentTransform()
		matrix:Invert()
		g:ApplyMatrix(matrix)
		
	end
	g:ApplyMatrix(self.matrix)	
end

--TODO: create some hidden interface for the following method (for now it only can be called from script, using AE_LuaConsole for example)

function RS_Picker:ExportShape(moho)
	local mesh = moho:Mesh()
	if mesh:CountCurves() < 1 then return end
	local picPath = LM.GUI.SaveFile("Enter name for the shape (it would be saved to resource folder)")
	if not picPath then return end
	for token in string.gmatch(picPath, "[^\\/]+") do picName = token end
	local outString = "{"
	for i = 0, mesh:Curve(0):CountPoints() - 1 do
		local point = mesh:Curve(0):Point(i)
		local pos = point.fPos
		outString = outString .. string.format("{%.3f,%.3f},", pos.x, pos.y)
	end
	outString = outString .. "}"
	picPath = self.resourcePath .. picName
	local file = io.open(picPath, "w")
	io.output(file)
	io.write(outString)
	io.close()
end

-- **************************************************
-- Utility methods for evaluation
-- **************************************************

function RS_Picker.Print(moho, message)
	-- simple call for debug
	print(message)
end

function RS_Picker.OrderUp(moho, name)
	-- simple call for debug
	local group = moho:LayerAsGroup(moho.layer)
	if not group then return end
	group:EnableLayerOrdering(true)
	local orderChannel = group:GetLayerOrdering()
	
	local foundLayer = nil
	for i=0, group:CountLayers() - 1 do
		local nextLayer = group:Layer(i)
		if nextLayer:Name() == name then 
			foundLayer = nextLayer
			break
		end
	end
	if not foundLayer then return end 
	
	local currentNumber = 0
	local currentOrder = orderChannel.value:Buffer()
	local currentOrderTable = {}
	for token in string.gmatch(currentOrder, "[^|]+") do
	   table.insert(currentOrderTable, token)
	   if token == foundLayer:UUID() then currentNumber = #currentOrderTable end
	end
	if currentNumber < 1 then return end
	table.remove(currentOrderTable, currentNumber)
	table.insert(currentOrderTable, foundLayer:UUID())
	local newOrder = table.concat(currentOrderTable, '|')
	orderChannel:SetValue(moho.layerFrame, newOrder)
	moho:SetCurFrame(moho.frame)
end